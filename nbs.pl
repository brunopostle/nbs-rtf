#!/usr/bin/perl

use strict;
use warnings;

die "NBS RTF to HTML converter
Usage $0 file1.rtf file2.rtf [...]
Creates files called file1.html file2.html [...]" unless scalar @ARGV;

use RTF::HTML::Converter;

undef $/;

for my $file (@ARGV)
{
    my $data;
    my $self = new RTF::HTML::Converter (Output => \$data);
    $self->parse_stream ($file);

# join split paragraphs
$data =~ s/ <\/p>[[:space:]]*<p>([[:alnum:]][^\t]*? <\/p>)/ $1/gs;
$data =~ s/ <\/p>[[:space:]]*<p>([[:alnum:]][^\t]*? <\/p>)/ $1/gs;
$data =~ s/ <\/p>[[:space:]]*<p>([[:alnum:]][^\t]*? <\/p>)/ $1/gs;
$data =~ s/ <\/p>[[:space:]]*<p>([[:alnum:]][^\t]*? <\/p>)/ $1/gs;

$data =~ s/([[:alnum:]]) <\/p>[[:space:]]*<p>([a-zA-Z])/$1 $2/gs;

# join and remove page headers
$data =~ s/(<p>.*? )<\/p>[[:space:]]*<p>([[:alnum:]][^\t]*?<\/p>)/$1 %%%% $2/g;
$data =~ s/<p>.*?<\/p>//g;

# fix for xml validity
$data =~ s/<!DOCTYPE.*?>[[:space:]]</</;
$data =~ s/<\/b><\/i><\/body>/<\/body>/;
$data =~ s/&#19;//g;


$data =~ s/<body>/<head><meta http-equiv="Content-Type" content="text\/html; charset=us-ascii" \/><\/head><body>/;

# bold paragraphs are h2
$data =~ s/<p><b>(.*?)<\/b><\/p>/<h2>$1<\/h2>/g;
# numbered lines are h3
$data =~ s/<p>[[:space:]]*([0-9]+)[[:space:]]*(.*?)<\/p>/<h3>$1 $2<\/h3>/g;


$data =~ s/(<p>-.*?<\/p>[[:space:]]*<p>)([[:space:]]*[[:alnum:]].*?<\/p>)/$1-$2/g;

# second level lists
$data =~ s/<p>-[[:space:]](.*?)<\/p>/<li>$1<\/li>/g;
$data =~ s/((?<li>.*?<\/li>[[:space:]]*)+)/\n<ul>\n$1<\/ul>\n\n/g;

# first level lists
$data =~ s/<p>[[:space:]]*&#34;--[[:space:]]*(.*?)<\/p>/<li>$1<\/li>/g;
$data =~ s/(<\/h[23]>)(.*?)(<h[23]>)/$1\n<ul>$2<\/ul>\n$3/gs;

$data =~ s/<ul>[[:space:]]*<\/ul>/\n/gs;

# other block level
$data =~ s/<ul>[[:space:]]*(<p>.*?<\/p>)[[:space:]]*<\/ul>/$1/gs;

$data =~ s/(Density:.*?kg\/m)`/$1&#179;/g;
$data =~ s/W\/m`K/W\/m&#178;K/g;
$data =~ s/([0-9])`C/$1&#176;C/g;
$data =~ s/Accuracy of reading: `/Accuracy of reading: &#177;/g;
$data =~ s/L\/m`/L\/m&#178;/g;
$data =~ s/m`\/L/m&#178;\/L/g;
$data =~ s/` angle/&#176; angle/g;
$data =~ s/Agr`ment/Agr&#233;ment/g;
$data =~ s/g\/m`/g\/m&#178;/g;
$data =~ s/m` of roof/m&#178; of roof/g;
$data =~ s/m`/m&#178;/g;
$data =~ s/ECOSE`/ECOS&#201;E/g;
$data =~ s/ECOS`E/ECOS&#201;E/g;
$data =~ s/Earthwool`/Earthwool&#174;/g;

# spaces
$data =~ s/[[:space:]]+</</gs;
$data =~ s/>[[:space:]]+/>/gs;
$data =~ s/></>\n\n</g;

# misc
$data =~ s/<p>.*?[[:space:]]Page[0-9[:space:]]+of[0-9[:space:]]+.*?<\/p>//g;

    $file =~ s/\.rtf/.html/i;
    open FILE, ">$file";
    print FILE $data;
    close FILE;
}

